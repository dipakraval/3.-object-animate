import './style.css'
import * as THREE from 'three'
import { OrbitControls } from 'three/examples/jsm/controls/OrbitControls.js'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader'
import * as dat from 'dat.gui'


const canvas = document.querySelector(".webgl");

/**
 * Scene ------------------------------------------------------------------------------Scean------------------------------------------------------
 */
 const scene = new THREE.Scene();
 scene.background = new THREE.Color(0x000000);
 

/**
 * Plane
 */
const planeGeometry = new THREE.PlaneGeometry(25, 25)
const floorTexture = new THREE.TextureLoader().load('textures/floor.jpg')
const plane = new THREE.Mesh(
    planeGeometry,
    new THREE.MeshPhongMaterial({ map: floorTexture })
)
plane.rotateX(-Math.PI / 2)
// plane.receiveShadow = true
scene.add(plane)


/**
 * Model Load
 */
 let stacy_txt = new THREE.TextureLoader().load('https://s3-us-west-2.amazonaws.com/s.cdpn.io/1376484/stacy.jpg');
 stacy_txt.flipY = false;
 const stacy_mtl = new THREE.MeshPhongMaterial({
    map: stacy_txt,
    color: 0xffffff,
    skinning: true 
});

let waist,neck,possibleAnims,idle;
let currentlyAnimating = false;
let mixer = new THREE.AnimationMixer();
const loader = new GLTFLoader();

// Load a glTF resource
loader.load(
    // resource URL
    'model/stacy_lightweight.glb',
    // called when the resource is loaded
    function ( gltf ) {
        
        let model = gltf.scene;
        let fileAnimations = gltf.animations;

        model.traverse(o => {

            if (o.isMesh) {
                
                o.castShadow = true;
                o.receiveShadow = true;
                o.material = stacy_mtl;
            }
            // Reference the neck and waist bones
            if (o.isBone && o.name === 'mixamorigNeck') {
              neck = o;
            }
            if (o.isBone && o.name === 'mixamorigSpine') {
              waist = o;
            }
        });

        // model.scale.set(7, 7, 7);
        // model.position.y = -11;
        scene.add(model);
        
        mixer = new THREE.AnimationMixer(model);
        let clips = fileAnimations.filter(val => val.name !== 'idle');
        possibleAnims = clips.map(val => {
            let clip = THREE.AnimationClip.findByName(clips, val.name);
            clip.tracks.splice(3, 3);
            clip.tracks.splice(9, 3);
            
            clip = mixer.clipAction(clip);
            return clip;
        });
        let idleAnim = THREE.AnimationClip.findByName(fileAnimations, 'idle');
        idleAnim.tracks.splice(3, 3);
        idleAnim.tracks.splice(9, 3);
  
        idle = mixer.clipAction(idleAnim);
        idle.play();
      
    },
    // called while loading is progressing
    function ( xhr ) {

        console.log( ( xhr.loaded / xhr.total * 100 ) + '% loaded' );

    },
    // called when loading has errors
    function ( error ) {

        console.log( 'An error happened '+error );

    }
);


/**
 * Lights
 */
const light1 = new THREE.SpotLight()
light1.position.set(2.5, 5, 2.5)
light1.angle = Math.PI / 8
light1.penumbra = 0.5
light1.castShadow = true
light1.shadow.mapSize.width = 1024
light1.shadow.mapSize.height = 1024
light1.shadow.camera.near = 0.5
light1.shadow.camera.far = 20
scene.add(light1)

const light2 = new THREE.SpotLight()
light2.position.set(-2.5, 5, 2.5)
light2.angle = Math.PI / 8
light2.penumbra = 0.5
light2.castShadow = true
light2.shadow.mapSize.width = 1024
light2.shadow.mapSize.height = 1024
light2.shadow.camera.near = 0.5
light2.shadow.camera.far = 20
scene.add(light2)



/**
 * Camera
 */
const size = {
    width: window.innerWidth,
    height: window.innerHeight
}
const camera = new THREE.PerspectiveCamera(75,size.width / size.height,0.1,1000);
camera.position.set(0, 1.4, 4.0)

scene.add( camera );





/**
 * OrbitControls
*/ 
const control = new OrbitControls(camera, canvas);
control.enableZoom = true;
control.enableDamping = true;

control.minDistance = 4;
control.maxDistance = 10;
control.minPolarAngle = Math.PI/5; // radians
// control.maxPolarAngle = Math.PI; // radians
control.maxPolarAngle = Math.PI/2.2; 


/**
 * Renderer
*/
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(size.width,size.height);


/**
 * Animation
 */
const clock = new THREE.Clock()
const tick = () => {
    if (mixer) {
        mixer.update(clock.getDelta());
    }
    control.update();
    renderer.render(scene,camera);
    window.requestAnimationFrame(tick);

}
tick();

$(document).ready(function(){
    $(".click").click(function(){
        playOnClick();
    });
});

function playOnClick() {
    let anim = Math.floor(Math.random() * possibleAnims.length) + 0;
    playModifierAnimation(idle, 0.25, possibleAnims[anim], 0.25);
}

function playModifierAnimation(from, fSpeed, to, tSpeed) {
    to.setLoop(THREE.LoopOnce);
    to.reset();
    to.play();
    from.crossFadeTo(to, fSpeed, true);
    setTimeout(function () {
      from.enabled = true;
      to.crossFadeTo(from, tSpeed, true);
      currentlyAnimating = false;
    }, to._clip.duration * 1000 - (tSpeed + fSpeed) * 1000);
}


document.addEventListener('mousemove', function (e) {


    const mq = window.matchMedia( "(max-width: 991px)" );
    if (mq.matches) {
    } else {
        var mousecoords = getMousePos(e);
        if (neck && waist) {
            moveJoint(mousecoords, neck, 50);
            moveJoint(mousecoords, waist, 30);
        }
    }

    
});

function getMousePos(e) {
    return { x: e.clientX, y: e.clientY };
}

function moveJoint(mouse, joint, degreeLimit) {
    let degrees = getMouseDegrees(mouse.x, mouse.y, degreeLimit);
    joint.rotation.y = THREE.Math.degToRad(degrees.x);
    joint.rotation.x = THREE.Math.degToRad(degrees.y);
}

function getMouseDegrees(x, y, degreeLimit) {
    let dx = 0,
    dy = 0,
    xdiff,
    xPercentage,
    ydiff,
    yPercentage;

    let w = { x: window.innerWidth, y: window.innerHeight };

    // Left (Rotates neck left between 0 and -degreeLimit)
    // 1. If cursor is in the left half of screen
    if (x <= w.x / 2) {
      // 2. Get the difference between middle of screen and cursor position
      xdiff = w.x / 2 - x;
      // 3. Find the percentage of that difference (percentage toward edge of screen)
      xPercentage = xdiff / (w.x / 2) * 100;
      // 4. Convert that to a percentage of the maximum rotation we allow for the neck
      dx = degreeLimit * xPercentage / 100 * -1;
    }

    // Right (Rotates neck right between 0 and degreeLimit)
    if (x >= w.x / 2) {
      xdiff = x - w.x / 2;
      xPercentage = xdiff / (w.x / 2) * 100;
      dx = degreeLimit * xPercentage / 100;
    }
    // Up (Rotates neck up between 0 and -degreeLimit)
    if (y <= w.y / 2) {
      ydiff = w.y / 2 - y;
      yPercentage = ydiff / (w.y / 2) * 100;
      // Note that I cut degreeLimit in half when she looks up
      dy = degreeLimit * 0.5 * yPercentage / 100 * -1;
    }
    // Down (Rotates neck down between 0 and degreeLimit)
    if (y >= w.y / 2) {
      ydiff = y - w.y / 2;
      yPercentage = ydiff / (w.y / 2) * 100;
      dy = degreeLimit * yPercentage / 100;
    }
    return { x: dx, y: dy };
}
